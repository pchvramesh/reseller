import {AppRegistry} from 'react-native';
import Layout from './src/pages/Layout';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => Layout);
