const constants = {
    basePath: 'http://localhost:8030/api',
    endPoints: new Map([
        ['Login', {path: '/auth', methods: ['post', 'put', 'get', 'patch']}]
    ])
};

export default constants;
