import Constants from './Constants';

const AppAPI = {};


Constants.endPoints.forEach((element, key) => {
    AppAPI[key] = {};
    element.methods.forEach((method) => {
        AppAPI[key][method] = (params, payload) => fetcher(method, key, element.path, params, payload)
    })
});

function fetcher(method, key, endpoint, params, body) {
    return new Promise(async (resolve, reject) => {
        // Build request
        const req = {
            method: method.toUpperCase(),
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
        };

        if (body) {
            req.body = JSON.stringify(body);
        }

        let thisUrl = Constants.basePath + endpoint;

        if (params) {
            thisUrl = thisUrl + params;
        }


        // Make the request
        return fetch(thisUrl, req)
            .then(async (response) => {
                let responseJson = {};
                try {
                    responseJson = await response.json();
                } catch (e) {
                    throw response;
                }

                if (response && response.status === 200) {
                    return responseJson;
                }
                throw responseJson;
            })
            .then((res) => {
                resolve(res)
            })
            .catch((err) => {
                reject({
                    code: err.status,
                    serverMessage: err.statusText
                })
            });
    });
}

export default AppAPI;
