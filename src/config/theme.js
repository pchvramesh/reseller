const theme = {
    statusBar: '#272760',

    primaryColor: '#6b52ae',
    secondaryColor: '#c4cfd5',

    primaryBG: '',
    secondaryBG: '',

    primaryText: 'white'
};

export default theme;
