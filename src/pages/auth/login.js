import React from 'react';
import {ScrollView, Text, View} from 'react-native';
import {connect} from "react-redux";
import styles from '../../styles';
import {Body, Card, CardItem, Container, Content} from "native-base";

class LoginPage extends React.Component {
    render() {
        return <Container>
            <Content>
                <View style={styles.container}>
                    <Card>
                        <CardItem>
                            <Body>
                            <Text>
                                Login page
                            </Text>
                            </Body>
                        </CardItem>
                    </Card>
                </View>
            </Content>
        </Container>
    }
}


const mapStateToProps = state => ({
    auth: state.auth
});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);
