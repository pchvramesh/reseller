import React from 'react';
import {Image, ScrollView, View} from 'react-native';
import {connect} from "react-redux";
import styles from '../../styles';
import {Body, Button, Card, CardItem, Left, Right, Text, Thumbnail} from "native-base";
import Icon from 'react-native-vector-icons/FontAwesome';

class HomePage extends React.Component {
    render() {
        return <ScrollView>
            <View styles={styles.container}>
                {
                    [1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0].map(()=> <View style={{margin: 10}}>
                        <Card>
                            <CardItem cardBody>
                                <Image
                                    source={{uri: 'https://1zmwq81820r61pswmu19upqf-wpengine.netdna-ssl.com/wp-content/uploads/2019/02/dummy.png'}}
                                    style={{height: 200, width: null, flex: 1}}/>
                            </CardItem>
                            <CardItem>
                                <View style={{flex: 1, flexDirection: 'row', justifyContent:'flex-end'}}>
                                    <Icon name="heart" size={24} style={{marginRight: 10}}/>
                                    <Icon name="bookmark" size={24} style={{marginRight: 10}}/>
                                    <Icon name="share-alt" size={24}/>
                                </View>
                            </CardItem>
                        </Card>
                    </View>)
                }
            </View>
        </ScrollView>
    }
}


const mapStateToProps = state => ({
    home: state.home
});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);
