import React, {Component} from 'react';
import {Provider} from 'react-redux'
import {createAppContainer, createDrawerNavigator, createStackNavigator, DrawerItems} from 'react-navigation'

import store from '../store';

import theme from '../config/theme';

import HeaderTitle from './common/header';
import Drawer from './common/drawer';

import HomePage from '../pages/home';
import LoginPage from '../pages/auth/login';
import {Container, Text, View} from "native-base";
import {SafeAreaView, ScrollView, StatusBar} from "react-native";


const AuthNavigation = createStackNavigator(
    {
        Login: {
            screen: LoginPage
        }
    },
    {
        headerMode: 'none',
        initialRouteName: 'Login',
    }
);

const HomeNavigation = createStackNavigator(
    {
        HomePage: {
            screen: HomePage,
            navigationOptions: {
                headerTitle: <HeaderTitle title={`Home`} color={theme.primaryText}/>,
                headerStyle: {
                    backgroundColor: theme.primaryColor,
                }
            }
        }
    },
    {
        initialRouteName: 'HomePage',
    }
);

const CustomDrawerContentComponent = props => (
    <ScrollView>
        <SafeAreaView forceInset={{top: 'always', horizontal: 'never'}}>
            <View>
                <Text>Drawer Header</Text>
            </View>
            <DrawerItems {...props} />
        </SafeAreaView>
    </ScrollView>
);

const DrawerNavigator = createDrawerNavigator(
    {
        Home: {
            screen: HomeNavigation,
            navigationOptions: {
                drawerLabel: 'Home ',
            }
        },
        Page: {
            screen: HomeNavigation,
            navigationOptions: {
                drawerLabel: 'Page ',
            }
        }
    },
    {
        hideStatusBar: false,
        drawerBackgroundColor: 'rgba(255,255,255,.9)',
        overlayColor: '#9289d5',
        contentOptions: {
            activeTintColor: '#fff',
            activeBackgroundColor: '#6b52ae',
        },
        contentComponent: Drawer
    }
);


const PrimaryNav = createStackNavigator({
    AuthNav: {screen: AuthNavigation},
    DrawerNav: {screen: DrawerNavigator}
}, {
    headerMode: 'none',
    initialRouteName: 'DrawerNav',
});

const AppContainer = createAppContainer(PrimaryNav);


export default class Layout extends Component {

    handleNavigationChange = (event) => {

    };

    render() {
        return (
            <Container>
                <StatusBar backgroundColor={theme.statusBar} barStyle="light-content"/>
                <Provider store={store}>
                    <AppContainer onNavigationStateChange={this.handleNavigationChange}/>
                </Provider>
            </Container>
        );
    }
}
