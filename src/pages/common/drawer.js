import {SafeAreaView, ScrollView, StatusBar} from "react-native";
import {Text, View} from "native-base";
import {DrawerItems} from "react-navigation";
import React from "react";

class Drawer extends React.Component {
    render() {
        return (<ScrollView>
                <SafeAreaView forceInset={{top: 'always', horizontal: 'never'}}>
                    <View>
                        <Text>Drawer Header</Text>
                    </View>
                    <DrawerItems {...this.props} />
                </SafeAreaView>
            </ScrollView>
        )
    }
}

export default Drawer;
