import React from 'react';
import {Text, View} from "native-base";

export default class HeaderTitle extends React.Component {
    render() {
        const {color, title} = this.props;
        return (
            <View style={{padding: 10}}>
                <Text style={{
                    color: color,
                    fontFamily: 'roboto',
                    fontWeight: 'bold',
                    fontSize: 20,
                }}>{title}</Text>
            </View>
        );
    }
}
