import {StyleSheet} from "react-native";
import theme from './config/theme';

export default StyleSheet.create({
    container: {
        flex: 1,
        padding:10
    }
});
